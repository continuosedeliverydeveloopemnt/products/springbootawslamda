package com.amazonaws.serverless.sample.springboot2.model;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

class PetTest {

	@Test
	void shouldExist () {
		assertNotNull(new Pet("0", "Afghan Hound", "Bella", PetData.getRandomDoB()));
	}

}

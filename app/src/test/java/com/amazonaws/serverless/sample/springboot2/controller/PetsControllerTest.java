package com.amazonaws.serverless.sample.springboot2.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import com.amazonaws.serverless.sample.springboot2.StreamLambdaHandler;

class PetsControllerTest {

	@Test
	void shouldExist() {
		assertNotNull(new PetsController());
	}

}

package com.amazonaws.serverless.sample.springboot2;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.AfterAll;
import datadog.trace.api.civisibility.DDTestSession;
import datadog.trace.api.civisibility.CIVisibility;

public class TagUnitTest {
	
	static DDTestSession testSession;
	
	@BeforeAll
	public static void setUp() throws Exception {
	     testSession = CIVisibility.startSession("pets", "junit5", null);
         testSession.setTag("test-level", "unit test");
	}
	
	@AfterAll
    static void tearDown() {
		testSession.end(null);
    }
	
	
}

/*
 * Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance
 * with the License. A copy of the License is located at
 *
 * http://aws.amazon.com/apache2.0/
 *
 * or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package com.amazonaws.serverless.sample.springboot2.controller;


import com.amazonaws.serverless.sample.springboot2.model.Pet;
import com.amazonaws.serverless.sample.springboot2.model.PetData;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.security.Principal;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;


@RestController
@EnableWebMvc
public class PetsController {
	
	private static final Logger logger = LogManager.getLogger(PetsController.class);
    @RequestMapping(path = "/pets", method = RequestMethod.POST)
    public Pet createPet(@RequestBody Pet newPet) {
        if (newPet.getName() == null || newPet.getBreed() == null) {
            return null;
        }

        Pet dbPet = newPet;
        dbPet.setId(UUID.randomUUID().toString());
        return dbPet;
    }

    @RequestMapping(path = "/pets", method = RequestMethod.GET)
    public Pet[] listPets(@RequestParam("limit") Optional<Integer> limit, Principal principal) {
        int queryLimit = 10;
        if (limit.isPresent()) {
            queryLimit = limit.get();
        }
        
    
        System.out.println("system out from get pets petscontroller");

       // System.err: One log statement but with a line break (AWS Lambda writes two events to CloudWatch).
        System.err.println("log data from stderr. \n this is a continuation of system.err");

        // Use log4j to log the same thing as above and AWS Lambda will log only one event in CloudWatch.
        logger.debug("log data from log4j debug \n this is continuation of log4j debug petscontroller ");

        logger.error("log data from log4j err. \n this is a continuation of log4j.err");

		logger.info("Serving lambda request."); 


        Pet[] outputPets = new Pet[queryLimit];
        outputPets[0] = new Pet("0", "Afghan Hound", "Bella", PetData.getRandomDoB());
        outputPets[1] = new Pet("1", "Norwegian Elkhound", "Rosie", PetData.getRandomDoB());

        return outputPets;
    }

    @RequestMapping(path = "/pets/{petId}", method = RequestMethod.GET)
    public Pet listPets() {
        Pet newPet =  new Pet("0", "Afghan Hound", "Bella", PetData.getRandomDoB());
        return newPet;
    }

}

import * as aws from "@pulumi/aws";
import * as pulumi from "@pulumi/pulumi";
import {usersFunction} from './UsersFunction'
import {usersExecutionRole} from "./UsersExecutionRole"
import {cluster} from "./ServerlessDb";
import {clusterInstance} from "./ServerlessDb";

const stack = pulumi.getStack();
usersFunction
usersExecutionRole
cluster
clusterInstance


const apigw = new aws.apigatewayv2.Api("httpApiGateway", {
  protocolType: "HTTP",
  name: "users"
});


const usersPermission = new aws.lambda.Permission("users-permission", {
  action: "lambda:InvokeFunction",
  principal: "apigateway.amazonaws.com",
  function: usersFunction,
  sourceArn: pulumi.interpolate`${apigw.executionArn}/*/*`,
}, {dependsOn: [apigw, usersFunction]});

const usersIntegration = new aws.apigatewayv2.Integration("users-integration", {
  apiId: apigw.id,
  integrationType: "AWS_PROXY",
  integrationUri: usersFunction.arn,
  integrationMethod: "POST",
  payloadFormatVersion: "1.0",
  passthroughBehavior: "WHEN_NO_MATCH",
});



const usersRoute = new aws.apigatewayv2.Route("users-route", {
  apiId: apigw.id,
  routeKey: "$default",
  target: pulumi.interpolate`integrations/${usersIntegration.id}`,
});

const apiGateWayLogGroup = new aws.cloudwatch.LogGroup("api-gateway-log-group", {name:stack + "api-gateway"} );

const apiGateWayFilter = new aws.cloudwatch.LogMetricFilter("api-gateway-filter", {
    pattern: "",
    logGroupName: apiGateWayLogGroup.name,
    metricTransformation: {
        name: "EventCount",
        namespace: "YourNamespace",
        value: "1",
    },
});

const stage = new aws.apigatewayv2.Stage("apiStage", {
  apiId: apigw.id,
  name: "$default",
  routeSettings: [
    {
      routeKey: usersRoute.routeKey,
      dataTraceEnabled: true,
      detailedMetricsEnabled: true,
      loggingLevel: "INFO",
      throttlingBurstLimit: 5000,
      throttlingRateLimit: 10000,
    },
  ],
  autoDeploy: true,
  accessLogSettings: {
	      destinationArn: apiGateWayLogGroup.arn,
	      format: '{ "requestId":"$context.requestId","extendedRequestId":"$context.extendedRequestId","ip":"$context.identity.sourceIp", "caller":"$context.identity.caller", "user":"$context.identity.user", "requestTime":"$context.requestTime", "httpMethod":"$context.httpMethod", "resourcePath":"$context.resourcePath", "status":"$context.status", "protocol":"$context.protocol","responseLength":"$context.responseLength" }'
	  }
 
}, {dependsOn: [usersRoute,apiGateWayLogGroup]});

export const url = pulumi.interpolate`${apigw.apiEndpoint}/${stage.name}`;
export const dbUrl = clusterInstance.endpoint
export const isWriteable = clusterInstance.writer
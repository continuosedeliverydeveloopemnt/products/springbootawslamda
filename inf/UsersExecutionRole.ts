import * as aws from "@pulumi/aws"
import * as pulumi from "@pulumi/pulumi"
import   {lamdaPolicy} from "./Policy"
import   {xrayPolicy} from "./Policy"



const stack = pulumi.getStack()

export const usersExecutionRole = new aws.iam.Role("users-execution-role", {assumeRolePolicy: `{
 
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}`,
 name:stack + '-' + 'users--execution-role',
 managedPolicyArns: [ lamdaPolicy.arn, xrayPolicy.arn]
}

);

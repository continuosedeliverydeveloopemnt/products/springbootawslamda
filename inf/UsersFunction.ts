import * as aws from "@pulumi/aws"
import * as pulumi from "@pulumi/pulumi";
import  {usersExecutionRole} from "./UsersExecutionRole"

const stack = pulumi.getStack()
const handler= "com.amazonaws.serverless.sample.springboot2.StreamLambdaHandler::handleRequest"
const functionName = stack + '-' +"usersFunction"


export const usersFunction = new aws.lambda.Function("users-function", {
    code: new pulumi.asset.FileArchive("../app/build/distributions/pets.zip"),   
    runtime: aws.lambda.Runtime.Java11,
    handler: handler,
    role: usersExecutionRole.arn,
    name: functionName,
    memorySize: 10240,
    timeout: 900,
    tracingConfig:{mode:"Active"},
    architectures:["arm64"],
    layers: [ "arn:aws:lambda:eu-north-1:901920570463:layer:aws-otel-java-agent-arm64-ver-1-26-0:2",
              "arn:aws:lambda:eu-north-1:464622532012:layer:dd-trace-java:11",
              "arn:aws:lambda:eu-north-1:464622532012:layer:Datadog-Extension-ARM:45"], 
    environment: {
        variables: {
			AWS_LAMBDA_EXEC_WRAPPER: "/opt/datadog_wrapper",
			DD_SITE: "datadoghq.eu",
			DD_ENV: stack,
			DD_API_KEY: "9fa2ebdddb1632f6d0d5a5df856ec554",
			DD_TAGS:"service:pets dd_sls_ci:v2.14.0",
			DD_TRACE_ENABLED:"true"	,
			DD_VERSION:"0.1",
			DD_SERVICE:"pets",
			OTEL_PROPAGATORS: "xray",
			DD_CAPTURE_LAMBDA_PAYLOAD: "false",
            DD_MERGE_XRAY_TRACES: "false",
            DD_FLUSH_TO_LOG: "true",
            DD_LOGS_INJECTION: "true"
		}
			
    }

},
   )
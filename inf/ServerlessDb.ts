import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";

const stack = pulumi.getStack();

export const cluster = new aws.rds.Cluster("cluster", {
    clusterIdentifier: "example",
    engine: "aurora-postgresql",
    engineMode: "provisioned",
    engineVersion: "13.6",
    databaseName: stack + "Database",
    masterUsername: "steinko",
    masterPassword: "DavidBowie1!",
    serverlessv2ScalingConfiguration: {
        maxCapacity: 1,
        minCapacity: 0.5,
    },
})

export const clusterInstance = new aws.rds.ClusterInstance("cluster-instance", {
    clusterIdentifier: cluster.id,
    instanceClass: "db.serverless",
    engine: "aurora-postgresql",
    engineVersion: cluster.engineVersion,
    identifier: stack + "-cluster-instance"
})